// Convert xml to xml -> object, object -> json
const xmlConverter = require ('./xml2json');
// Parse XML module
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var DOMParser = require('domparser').DOMParser;
// Filesystem works module
var fs = require('fs');
// Manifests folders
var path = [];
path.push('/home/tatumars/Документы/Program/manifest/v1.mpd');//(process.argv[2]);
path.push('/home/tatumars/Документы/Program/manifest/v3.mpd');//(process.argv[3]);
// JSON file with rules
path.push('/home/tatumars/Документы/Program/manifest/comparison_params.json');//(process.argv[4]);
// Check exist of filepath
getFiles(path);
path = readTextFile(path);
// Parse XML files and divide on attributes
var sample = { 'MPD' : xmlConverter.xml2json(parseDash(path[0]), 'object', '')};
var testing = { 'MPD' : xmlConverter.xml2json(parseDash(path[1]), 'object', '')};
// Parse json rules from object
var rules = { 'conditions' : JSON.parse(path[2])};

var result = [];
var path_size = [];
var pathSample = mapObjects(sample);
result = differentObject(sample, testing, rules.conditions, result, pathSample, path_size);
var res = JSON.stringify(result);
console.log(res);
var p = 0;


// Find different between sapmple and test .mpd files
function differentObject(sample, testing, conditions, result, p_param, path_size){
    var nc_sample = 0;
    var nc_testing = 0;
    var check_dif = null;
    var unexpected_parameters = 0;
    for (var s in sample){
        for (var t in testing){
            if (typeof (sample[s]) == 'string' && typeof (testing[t]) == 'string'){
                if (s == t){
                    if (sample[s] != testing[t]){
                        result.push({type: conditions.default_action, reason: 'comparison', parameter_path: p_param[path_size.length] + s, expected_value: sample[s], received_value: testing[t]});
                    }   
                    check_dif = true;
                    delete sample[s];
                    delete testing[t];
                    break;
                } else if (s != t){
                    check_dif = false;
                }
            } else if (typeof (sample[s]) == 'object' && typeof (testing[t]) == 'object'){
                if (s == t){
                    nc_sample = sample[s];
                    nc_testing = testing[t];
                    check_dif = 'new_child';
                    if (typeof (sample[s].length) !== 'undefined' && typeof (testing[t].length) == 'undefined'){
                        nc_sample = sample[s][0];
                        path_size.push(path_size.length + 1);
                    }
                }
            } else if (typeof (sample[s]) == 'object' && typeof (testing[t]) == 'string'){
                check_dif = false;
            }
            unexpected_parameters = testing;
        }
        if (check_dif == false){
            if (typeof (sample[s]) == 'object'){
                result.push({type: conditions.default_action, reason: 'missing_element', parameter_path: p_param[path_size.length + 1]});
                path_size.push(path_size.length + 1);
                delete sample[s];
            } else if (typeof (sample[s]) == 'string'){
                result.push({type: conditions.default_action, reason: 'missing_parameter', parameter_path: p_param[path_size.length] + s});
                delete sample[s];
            }
        }
        else if (check_dif == 'new_child'){
            for (var i in unexpected_parameters){
                if (typeof (unexpected_parameters[i]) == 'string'){
                    result.push({type: conditions.default_action, reason: 'unexpected_parameter', parameter_path: p_param[path_size.length] + s + '/' + i});
                    delete unexpected_parameters[i];
                }
            }
            path_size.push(path_size.length + 1);
            differentObject(nc_sample, nc_testing, conditions, result, p_param, path_size);
        }
    }
    return result;
}

// All path to object
function mapObjects(obj, asArrays){
    var paths = ['Paths:'];

    function scan(obj, stack){
        var k, v, path;

        for(k in obj){
            if(obj.hasOwnProperty(k)){
                
                path = stack.concat([k] + '/');
                if((v = obj[k]) !== null && typeof v == 'object'){
                    paths.push(path.join(''));
                    scan(v,path);
                }
            }
        }
    }
    scan(obj, []);
    return paths;
}

function conditionsRules(sample, testing, conditions){
    var default_action = conditions.default_action;
    var filter_sample = 0;
    var check = false;
    var count = 0;

    for (var i = 0; i < conditions.rules.length; i++){
        var mpd_path = 0;
        if (typeof (conditions.rules[i].path_contains) != 'undefined'){
            var path_return = checkContains(count, conditions.rules[i].path_contains, sample);
        }        
    }
 }

function checkContains(count, path_contains, mpd_object){
    for (var i = 0; i < path_contains.length; i++){
        for (var key in mpd_object){
            if (key == path_contains[i].element_name){
                count++;
                if (count == path_contains.length){
                    return mpd_object.key;
                }
                checkContains(count, path_contains, mpd_object.key);
            }
        }
    }
    if (count == 0)
    {
        //checkContains(count, path_contains, mpd_object.key);
    }      
}

// Read file
function readTextFile(file){
    var allText = [];
    for (var i = 0; i < file.length; i++)
    {
        file[i] = 'file://' + file[i];
        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", file[i], false);
        rawFile.onreadystatechange = function (){
            if(rawFile.readyState === 4){
                if(rawFile.status === 200 || rawFile.status == 0){
                    allText.push(rawFile.responseText);
                    alert(allText[i]);
                }
            }
        }
        rawFile.send(null);
    }
    return allText;
}

// Check folder
function getFiles (folder){
    for (var i = 0; i < folder.length; i++){
        if (!fs.existsSync(folder[i])){
            console.log('-- Check the path / file name is entered correctly --');
            process.exit(-1);
        }
    }
}

// XML parse
function parseDash(manifestString){
    if (manifestString === '') {
        throw new Error(errors.DASH_EMPTY_MANIFEST);
    }   
    const parser = new DOMParser();
    const xml = parser.parseFromString(manifestString, 'application/xml');
    const mpd = xml && xml.documentElement.tagName === 'MPD' ?
        xml.documentElement : null;
    
    if (!mpd || mpd &&
        mpd.getElementsByTagName('parsererror').length > 0) {
    throw new Error(errors.DASH_INVALID_XML);
    } 
    return mpd;
}